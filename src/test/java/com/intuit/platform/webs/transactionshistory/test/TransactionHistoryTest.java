package com.intuit.platform.webs.transactionshistory.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.never;
import static org.mule.module.http.api.client.HttpRequestOptionsBuilder.newOptions;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleMessage;
import org.mule.api.client.MuleClient;
import org.mule.config.spring.SpringRegistry;
import org.mule.tck.junit4.FunctionalTestCase;
import org.springframework.context.ApplicationContext;

import com.intuit.platform.webs.subscription.transactions.service.TransactionHistoryManager;
import com.intuit.schema.platform.webs.subscription.internal.transactions.v3.Transaction;

public class TransactionHistoryTest extends FunctionalTestCase {
	private static final BigInteger EXPECTED_CUSTOMER_ID_50 = BigInteger.valueOf(50L);
	private static final String RECORD_SAVE_SUCCESS = "record save success";
	private static final String EXPECTED_TRANS_ID = "t001";
	private static final String EXPECTED_TRANS_TYPE = "SIGNUP";
	private static final String EXPECTED_TRANS_STATUS = "COMPLETED";	
	private TransactionHistoryManager mocked;
	
	@Override
	protected String getConfigResources() {
		return "src/main/app/webs-subscription-transactions.xml,src/test/resources/transactions-test-endpoint-config.xml";
	}
	
	@BeforeClass 
	public static void globalSetup() { 
		System.setProperty("env", "dev");
		System.setProperty("dc", "qydc");
	}
		
	@Before
	public void setupMock() {
		ApplicationContext ac = (ApplicationContext)muleContext.getRegistry().lookupObject(SpringRegistry.SPRING_APPLICATION_CONTEXT);
		mocked = (TransactionHistoryManager) ac.getBean("transaction");
		when(mocked.persistTransaction(any(String.class), any(String.class), any(Transaction.class)))
			.thenReturn(RECORD_SAVE_SUCCESS);
	} 
	
	@Test
    public void testPersistTxSuccess() throws Exception
    {
		//MuleClient client = new MuleClient(muleContext);
		
		MuleClient client = muleContext.getClient();
      
		//loading the request xml from the test resources folder...
    	String inputlXML = loadResourceAsString("src/test/resources/txhistoryinput.xml");
    	
    	MuleMessage message = setMuleMessage(inputlXML);
		MuleMessage result = client.send("vm://transactions_inbound", message, 
				newOptions().disableStatusCodeValidation().responseTimeout(20000).build());
		System.out.println("after calling");
    	//client.send("vm://transactions_inbound", inputlXML, msgProp);

    	ArgumentCaptor<String> tidArgument = ArgumentCaptor.forClass(String.class);
    	ArgumentCaptor<String> typeArgument = ArgumentCaptor.forClass(String.class);
    	ArgumentCaptor<Transaction> transactionArgument = ArgumentCaptor.forClass(Transaction.class);
    	verify(mocked).persistTransaction(tidArgument.capture(), typeArgument.capture(), transactionArgument.capture());
    	
    	assertEquals(EXPECTED_TRANS_ID, tidArgument.getValue());
    	assertEquals(EXPECTED_CUSTOMER_ID_50, transactionArgument.getValue().getCustomerAccountID());
    	assertEquals(EXPECTED_TRANS_TYPE, typeArgument.getValue());
    	assertEquals(EXPECTED_TRANS_STATUS, transactionArgument.getValue().getTransactionStatus());
    	
    	MuleMessage dispatch2 = client.request("vm://transactions_outbound", 7000L);
    	assertNull(dispatch2);
    }

	// Test to see the PAYMENT_SUCCESS_RECURRING transaction types transaction
	// don't get saved in the DB
	@Test
	public void testPaymentFailureReuccringTransType() throws Exception {
		// MuleClient client = new MuleClient(muleContext);

		MuleClient client = muleContext.getClient();

		// loading the request xml from the test resources folder...
		String inputlXML = loadResourceAsString("src/test/resources/txhistoryinput.xml");

		// adding JMS header properties
		Map<String, Object> msgProp = new HashMap<String, Object>();
		msgProp.put("UserName", "Intuit.cto.SUBS_Service");
		msgProp.put("intuit_appid", "Intuit.cto.SUBS_Service");
		msgProp.put("intuit_offeringid", "Intuit.platform.webs.services.obill");
		msgProp.put("intuit_locale", "en_US");
		msgProp.put("intuit_country", "US");
		msgProp.put("intuit_webs_transaction_typeid",
				"PAYMENT_SUCCESS_RECURRING");
    	msgProp.put("intuit_tid", "t001");
		MuleMessage muleMessage = new DefaultMuleMessage("test", muleContext);
		muleMessage.addProperties(msgProp,org.mule.api.transport.PropertyScope.OUTBOUND);
		muleMessage.setPayload(inputlXML);
		MuleMessage result = client.send("vm://transactions_inbound", muleMessage, 
				newOptions().disableStatusCodeValidation().responseTimeout(20000).build());
		
    	ArgumentCaptor<String> tidArgument = ArgumentCaptor.forClass(String.class);
    	ArgumentCaptor<String> typeArgument = ArgumentCaptor.forClass(String.class);
    	ArgumentCaptor<Transaction> transactionArgument = ArgumentCaptor.forClass(Transaction.class);
		verify(mocked, never()).persistTransaction(tidArgument.capture(), typeArgument.capture(), transactionArgument.capture());
		System.out.println(" response message " + result);
		assertNull(result);
	}

	// Test to see the PAYMENT_FAILURE_RECURRING transaction types transaction
	// don't get saved in the DB
	@Test
	public void testPaymentSuccessReuccringTransType() throws Exception {
		// MuleClient client = new MuleClient(muleContext);

		MuleClient client = muleContext.getClient();

		// loading the request xml from the test resources folder...
		String inputlXML = loadResourceAsString("src/test/resources/txhistoryinput.xml");

		// adding JMS header properties
		Map<String, Object> msgProp = new HashMap<String, Object>();
		msgProp.put("UserName", "Intuit.cto.SUBS_Service");
		msgProp.put("intuit_appid", "Intuit.cto.SUBS_Service");
		msgProp.put("intuit_offeringid", "Intuit.platform.webs.services.obill");
		msgProp.put("intuit_locale", "en_US");
		msgProp.put("intuit_country", "US");
		msgProp.put("intuit_webs_transaction_typeid",
				"PAYMENT_FAILURE_RECURRING");
    	msgProp.put("intuit_tid", "t001");
		MuleMessage muleMessage = new DefaultMuleMessage("test", muleContext);
		muleMessage.addProperties(msgProp,org.mule.api.transport.PropertyScope.OUTBOUND);
		muleMessage.setPayload(inputlXML);
		MuleMessage result = client.send("vm://transactions_inbound", muleMessage, 
				newOptions().disableStatusCodeValidation().responseTimeout(20000).build());
		
    	ArgumentCaptor<String> tidArgument = ArgumentCaptor.forClass(String.class);
    	ArgumentCaptor<String> typeArgument = ArgumentCaptor.forClass(String.class);
    	ArgumentCaptor<Transaction> transactionArgument = ArgumentCaptor.forClass(Transaction.class);
		verify(mocked, never()).persistTransaction(tidArgument.capture(), typeArgument.capture(), transactionArgument.capture());
		System.out.println(" response message " + result);
		assertNull(result);
	}

	public MuleMessage setMuleMessage(String inputlXML) {

		MuleMessage muleMessage = new DefaultMuleMessage("test", muleContext);
		Map<String,Object> msgProp = new HashMap<String, Object>();
    	msgProp.put("UserName", "Intuit.cto.SUBS_Service");
    	msgProp.put("intuit_appid", "Intuit.cto.SUBS_Service");
    	msgProp.put("intuit_offeringid", "Intuit.platform.webs.services.obill");
    	msgProp.put("intuit_locale", "en_US");
    	msgProp.put("intuit_country", "US");
    	msgProp.put("intuit_tid", "t001");
    	msgProp.put("intuit_webs_transaction_typeid", "SIGNUP");

		muleMessage.addProperties(msgProp,org.mule.api.transport.PropertyScope.OUTBOUND);
		muleMessage.setPayload(inputlXML);
		return muleMessage;

	}

}
